function [gfv] = fun_gabor_fv(img,f,m,n)
% extract gabor feature from given image
% The following toolbox is used for feature extraction
% Simplegabor - Multiresolution Gabor Feature Toolbox
% http://www2.it.lut.fi/project/simplegabor/downloads/src/simplegabortb/
% img: input image
% f: the frequency of the highest frequency filter
% m: number of filter frequencies.
% n: number or filter orientations

if length(size(img))>2,
    img = rgb2gray(img); % convert to gray level image
end

% normalize image pixel to [0,1]
img = double(img);
minI = min(img(:));
maxI = max(img(:));
if abs(maxI-minI)>1e-4,
    a = 1/(maxI-minI);
    b = -a*minI;
    img = a*img+b;
else
    img = img/maxI;
end


% create gabor filter bank
bank=sg_createfilterbank(size(img), f , m, n);

% filter image
r=sg_filterwithbank(img,bank); % notice that there are tunable parameters

gMx=sg_resp2samplematrix(r); % Convert responses to matrix form
gMx = sg_normalizesamplematrix(gMx); % Normalise responses to reduce illumination effect
gfv = zeros(n*m*2,1);
gMx = abs(gMx);
gfv(1:n*m) = mean(mean(gMx)); % mean

[N1,N2] = size(img);
gMx = reshape(gMx,N1*N2,m*n); 

gfv(n*m+1:end) = std(gMx)';  % standard variation

