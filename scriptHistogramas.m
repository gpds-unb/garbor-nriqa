function H_all = scriptHistogramas(fv_all,codebook)

numClusters = size(codebook,2);
numImgs = size(fv_all);

for indiceImg = 1:numImgs 
    fv = fv_all{indiceImg,1};
    numPatches = size(fv,2);

    % Calcula o "Nearest Neighbor" entre os patches da imagem e do dicionário 
    % (codebook)
    [dist_min,min_id] = min(repmat(sum(fv.^2)',1,numClusters)+repmat(sum(codebook.^2),numPatches,1)-2*fv'*codebook,[],2);
%     min_id = zeros([1 numPatches]);
%     for i = 1:numPatches
%         dist_min = 10000000;
%         patch = fv(:,i)';
%         for j = 1:numClusters
%             cluster = codebook(:,j)';
%             dist = (sqrt(sum((patch-cluster).^2)));
%             if dist < dist_min
%                min_id(i) = j;
%                dist_min = dist;
%             end
%         end
%     end
    
    H = zeros(numClusters,1);
    for i=1:numPatches
       H(min_id(i),1) = H(min_id(i),1) + 1;  
    end
    total = sum(H);
    H = H/total;

%     H_all(:,indiceImg) = H;
    H_all(indiceImg,:) = H;
end