function [fv] = extrairGaborFeatures (img)

tamanhoPatch = 11;

if length(size(img))>2,
    img = rgb2gray(img); % convert to gray level image
end

[r c] = size(img);

% Ajustar imagem para n�o haver blocos incompletos
excedenteLinha = mod(r,tamanhoPatch);
excedenteColuna = mod(c,tamanhoPatch);
imgAjustada = img(1:r-excedenteLinha,1:c-excedenteColuna);

% Extrair patches
patches = im2col(imgAjustada,[11,11],'distinct');
[l qntPatch] = size(patches);

% Calcular gabor dos patches
for numPatch = 1:qntPatch
    fv(:,numPatch) = calcularGabor(patches(:,numPatch));
end

end

function fv = calcularGabor(patch)

% Transformar patch (vetor) em matriz
[patchEmBloco,pad] = vec2mat(patch,11);

% Calcular Gabor do patch de acordo com os par�metros do artigo
% orientacao(0,45,90,135) e 
% frequencia(1, 1/sqrt(2), 1/2, 1/(2*sqrt(2)), 1/4)
[fv] = fun_gabor_fv(patchEmBloco',1,5,4);

end