function codebook = concatenarDict(dict);

% dict, saida da fun��o scriptLIVE_dict
numImg = size(dict,1);
codebook = [];
for i=1:numImg
    codebook = cat(1,codebook,dict{i,1}'); 
end
