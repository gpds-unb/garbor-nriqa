function vetorFvLIVE = scriptLIVE_fv()
% Script para extrair as "Gabor Features" das imagens do Banco de Dados
% LIVE, o resultado � igual a variavel "fv_all" do algoritmo CBIQ
 
diretorioLIVE = 'C:\Users\gpds\Pictures\databaserelease2';
n_imgs = 1;
for i=1:1:227
   arquivoImagem = sprintf('%s\\jp2k\\img%d.bmp',diretorioLIVE,i);
   img = imread(arquivoImagem);
   imgFv = extrairGaborFeatures (img);
   vetorFvLIVE{n_imgs,:} = imgFv;
   n_imgs = n_imgs + 1;
end
for i=1:1:233
   arquivoImagem = sprintf('%s\\jpeg\\img%d.bmp',diretorioLIVE,i);
   img = imread(arquivoImagem);
   imgFv = extrairGaborFeatures (img);
   vetorFvLIVE{n_imgs,:} = imgFv;
   n_imgs = n_imgs + 1;
end
for i=1:1:174
   arquivoImagem = sprintf('%s\\wn\\img%d.bmp',diretorioLIVE,i);
   img = imread(arquivoImagem);
   imgFv = extrairGaborFeatures (img);
   vetorFvLIVE{n_imgs,:} = imgFv;
   n_imgs = n_imgs + 1;
end
for i=1:1:174
   arquivoImagem = sprintf('%s\\gblur\\img%d.bmp',diretorioLIVE,i);
   img = imread(arquivoImagem);
   imgFv = extrairGaborFeatures (img);
   vetorFvLIVE{n_imgs,:} = imgFv;
   n_imgs = n_imgs + 1;
end
for i=1:1:174
   arquivoImagem = sprintf('%s\\fastfading\\img%d.bmp',diretorioLIVE,i);
   img = imread(arquivoImagem);
   imgFv = extrairGaborFeatures (img);
   vetorFvLIVE{n_imgs,:} = imgFv;
   n_imgs = n_imgs + 1;
end