function [indicesImgsTreino, indicesImgsTeste] = indicesTreinamentoETesteLIVE(orgs,refnames_all)
totalImagens = 29;
totalImgTreino = 23;
% totalImgTeste = 6;

refImages = {'bikes.bmp','building2.bmp','buildings.bmp','caps.bmp',...
    'carnivaldolls.bmp','cemetry.bmp','churchandcapitol.bmp',...
    'coinsinfountain.bmp','dancers.bmp','flowersonih35.bmp',...
    'house.bmp','lighthouse.bmp','lighthouse2.bmp','manfishing.bmp',...
    'monarch.bmp','ocean.bmp', 'paintedhouse.bmp','parrots.bmp',...
    'plane.bmp','rapids.bmp','sailing1.bmp','sailing2.bmp',...
    'sailing3.bmp','sailing4.bmp','statue.bmp','stream.bmp',...
    'studentsculpture.bmp','woman.bmp','womanhat.bmp'};

indicesImgsRefPermutadas = randperm(totalImagens);
indicesImgsRefTreino = indicesImgsRefPermutadas(indicesImgsRefPermutadas(1:totalImgTreino));
indicesImgsRefTeste = indicesImgsRefPermutadas(indicesImgsRefPermutadas(totalImgTreino+1:totalImagens));

imgsRefTreino = refImages(indicesImgsRefTreino);
imgsRefTeste = refImages(indicesImgsRefTeste);

refnames_dist = refnames_all(~orgs);

% Pegar somente os indices das imagens distorcidas das imagens referÍncias
% selecionadas para treinamento
indicesImgsTreino = [];
for aux=1:size(imgsRefTreino,2)
    indicesImg = strfind(refnames_dist,char(imgsRefTreino(aux)));
    for i=1:size(indicesImg,2)
        if indicesImg{1,i} == 1
            indicesImgsTreino = [indicesImgsTreino i];
        end
    end
end

% Pegar somente os indices das imagens distorcidas das imagens referÍncias
% selecionadas para teste
indicesImgsTeste = [];
for aux=1:size(imgsRefTeste,2)
    indicesImg = strfind(refnames_dist,imgsRefTeste{1,aux});
    for i=1:size(indicesImg,2)
        if indicesImg{1,i} == 1
            indicesImgsTeste = [indicesImgsTeste i];
        end
    end
end