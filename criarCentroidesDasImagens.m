function vetorCentroides = criarCentroidesDasImagens(vetorFv,numCentroides)

numImgs = size(vetorFv,1);

for i=1:numImgs
   [idx, vetorCentroides{i,1}] = kmeans(vetorFv{i,1}',numCentroides,'MaxIter',10000);
end
