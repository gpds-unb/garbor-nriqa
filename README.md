# README #

Implementação da métrica CBIQ/CORNIA

"No-Reference Image Quality Assessment Using Visual Codebooks"

"Unsupervised feature learning framework for no-reference image quality assessment"

## Requisitos ##
* Necessário a biblioteca "Simplegabor - Multiresolution Gabor Feature Toolbox" (https://gitlab.com/gpds-unb/simple-gabor-toolbox-redistribution)

## Arquivos ##

### scriptMetrica.m ###
* Contém o passo a passo da métrica (Incompleto)