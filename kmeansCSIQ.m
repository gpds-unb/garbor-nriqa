function kmeansCSIQ(nomeDistorcao,qntPatchesPorImgParaAnalisar)

qntNiveisDst = 5;

nomeImagens = {'1600';'aerial_city';'boston';'bridge';'butter_flower';...
    'cactus';'child_swimming';'couple';'elk';'family';'fisher';'foxy';...
    'geckos';'lady_liberty';'lake';'log_seaside';'monument';'native_american';...
    'redwood';'roping';'rushmore';'shroom';'snow_leaves';'sunset_sparrow';...
    'sunsetcolor';'swarm';'trolley';'turtle';'veggies';'woman'};

distorcoes = {'AWGN';'blur';'contrast';'fnoise';'jpeg';'jpeg2000'};
[qntImg col] = size(nomeImagens);
[qntDst col] = size(distorcoes);

numDst = find (strcmp(distorcoes,nomeDistorcao));
for numImg = 8:1:qntImg
    for nivelDst = 1:1:qntNiveisDst
        nomeFv = sprintf('.\\Patches\\%s.%s.%d.mat',...
            nomeImagens{numImg},...
            distorcoes{numDst},nivelDst);
        load(nomeFv);
        if (nargin() == 2)
            indicesRand = randperm(size(imgFv,2));
            imgFv = imgFv(:,indicesRand(1:qntPatchesPorImgParaAnalisar));
        end
        [idx centroides] = kmeans(imgFv',200,'MaxIter',1000);
        nomeMat = sprintf('.\\Clusters\\200clusters_%s.%s.%d.mat',...
            nomeImagens{numImg},...
            distorcoes{numDst},nivelDst);
        save(nomeMat,'centroides');
        display(sprintf('Realizado kmeans da Imagem: %s.%s.%d !',...
            nomeImagens{numImg},...
            distorcoes{numDst},nivelDst));
    end
end