function scriptMetrica(dmos,refnames_all,orgs)

% Em construcao

% Constantes
qntCentroidePorImagens = 200;
tamanhoDicionario = 10000;
porcentagemTreino = 0.7;

%% Criar Codebook --------------------------------------------------------

% Extrair as características Gabor de todas as imagens do banco de dados
% CSIQ
vetorFvCSIQ = scriptCSIQ_fv();

% Extrai os centroides das imagens
vetorCentroides = criarCentroidesDasImagens(vetorFvCSIQ,qntCentroidePorImagens);

% Cria o dicionário geral
matrizComCentroidesDasImgs = concatenarCelulas(vetorCentroides);
[idx, codebook] = kmeans(matrizComCentroidesDasImgs,tamanhoDicionario);

% Utilizar Métrica no LIVE -----------------------------------------------

% Extrair as características Gabor de todas as imagens do banco de dados
% LIVE
vetorFvLIVE = scriptLIVE_fv();

% % Extrai os centroides das imagens
% vetorCentroides = criarCentroidesDasImagens(vetorFvCSIQ,qntCentroidePorImagens);

% Criar os histogramas das imagens, comparando com o dicionário
vetorHistogramas = scriptHistogramas(vetorFvLIVE,codebook');
histogramasDist = vetorHistogramas(~orgs,:);

% Treinar SVM
[indicesImgsTreino, indicesImgsTeste] = indicesTreinamentoETesteLIVE(orgs,refnames_all);

dmosDist = dmos(~orgs);
dmosTreino = dmosDist(indicesImgsTreino);
histogramasTreino = histogramasDist(indicesImgsTreino,:);

dmosTeste = dmosDist(indicesImgsTeste);
histogramasTeste = histogramasDist(indicesImgsTeste,:);

model = svmtrain(dmosTreino', histogramasTreino , '-s 4 -t 0');

% Testar SVM
[predicted_label] = svmpredict(dmosTeste', histogramasTeste, model);

% Correlações das imagens Testes
corr_spearman = corr(dmosTeste(:),predicted_label(:),'type','Spearman');
display(sprintf('Correlacao Spearman: %d',corr_spearman));
