function centroides = codebookCSIQPorDistorcao(nomeDistorcao,tamanhoDicionario,qntPatchesPorImgParaAnalisar)
    % Script para calcular os Gabor Features das imagens 
    % do banco de dados CSIQ

    qntNiveisDst = 5;
    diretorioDatabase = 'C:\Users\gpds\Pictures\dst_imgs\';

    nomeImagens = {'1600';'aerial_city';'boston';'bridge';'butter_flower';...
        'cactus';'child_swimming';'couple';'elk';'family';'fisher';'foxy';...
        'geckos';'lady_liberty';'lake';'log_seaside';'monument';'native_american';...
        'redwood';'roping';'rushmore';'shroom';'snow_leaves';'sunset_sparrow';...
        'sunsetcolor';'swarm';'trolley';'turtle';'veggies';'woman'};

    distorcoes = {'AWGN';'blur';'contrast';'fnoise';'jpeg';'jpeg2000'};
    % distorcoes = {'AWGN';'blur';'fnoise';'jpeg';'jpeg2000'};
    [qntImg col] = size(nomeImagens);
    [qntDst col] = size(distorcoes);

    n_imgs = 1;
    numDst = find (strcmp(distorcoes,nomeDistorcao));
    for numImg = 1:1:qntImg
        for nivelDst = 1:1:qntNiveisDst
            if (nargin() == 1)
                nomeMat = sprintf('.\\Clusters\\200clusters_%s.%s.%d.mat',...
                    nomeImagens{numImg},...
                    distorcoes{numDst},nivelDst);
            else
                nomeMat = sprintf('.\\Clusters\\200clusters_%dPatches_%s.%s.%d.mat',...
                    qntPatchesPorImgParaAnalisar,nomeImagens{numImg},...
                    distorcoes{numDst},nivelDst);
            end
            load(nomeMat);
            vetorCentroides{n_imgs,1} = centroides;
            n_imgs = n_imgs +1;
        end
    end
    matrizConcatenadaComCentroides = concatenarCelulas(vetorCentroides);
    [idx, centroides] = kmeans(matrizConcatenadaComCentroides,tamanhoDicionario,'MaxIter',10000);
    if (nargin() == 1)
        save(sprintf('codebook%s_%dClusters.mat',nomeDistorcao,tamanhoDicionario),'centroides');
    else
        save(sprintf('codebook%s_%dClusters_%dPatches.mat',nomeDistorcao,tamanhoDicionario,qntPatchesPorImgParaAnalisar),'centroides');
    end
end