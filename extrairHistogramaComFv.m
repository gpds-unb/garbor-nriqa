function H = extrairHistogramaComFv(fv,codebook)

numClusters = size(codebook,2);
numPatches = size(fv,2);

% Calcula o "Nearest Neighbor" entre os patches da imagem e do dicionário 
% (codebook)
[dist_min,min_id] = min(repmat(sum(fv.^2)',1,numClusters)+repmat(sum(codebook.^2),numPatches,1)-2*fv'*codebook,[],2);

H = zeros(numClusters,1);
for i=1:numPatches
   H(min_id(i),1) = H(min_id(i),1) + 1;  
end