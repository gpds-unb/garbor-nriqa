function vetorFv_CSIQ = scriptCSIQ_fv();
% Script para calcular os Gabor Features das imagens 
% do banco de dados CSIQ

qntNiveisDst = 5;
diretorioDatabase = 'C:\Users\gpds\Pictures\dst_imgs\';

nomeImagens = {'1600';'aerial_city';'boston';'bridge';'butter_flower';...
    'cactus';'child_swimming';'couple';'elk';'family';'fisher';'foxy';...
    'geckos';'lady_liberty';'lake';'log_seaside';'monument';'native_american';...
    'redwood';'roping';'rushmore';'shroom';'snow_leaves';'sunset_sparrow';...
    'sunsetcolor';'swarm';'trolley';'turtle';'veggies';'woman'};

distorcoes = {'AWGN';'blur';'contrast';'fnoise';'jpeg';'jpeg2000'};

[qntImg col] = size(nomeImagens);
[qntDst col] = size(distorcoes);

n_imgs = 1;
for numDst = 1:1:qntDst
    for numImg = 1:1:qntImg
        for nivelDst = 1:1:qntNiveisDst
            nomeArquivo = sprintf('%s%s\\%s.%s.%d.png',diretorioDatabase,...
                distorcoes{numDst},...
                nomeImagens{numImg},...
                distorcoes{numDst},nivelDst);
            imgDst = imread(nomeArquivo);
            imgFv = extrairGaborFeatures (imgDst);
            vetorFv_CSIQ{n_imgs,:} = imgFv;
            n_imgs = n_imgs + 1;
        end
    end
end
% save('vetorFv_CSIQ.mat','vetorFv_CSIQ');