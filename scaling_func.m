function [fvScale,s1,s2] = scaling_func(data_train)
% Find scaling parameter for each dimension

[N, M] = size(data_train);
fvScale = zeros(N,M);
% scale feature value along each dimenstion to the range [-1,1]
s1 = zeros(N,1);
s2 = zeros(N,1);
for i = 1:N,
    row = data_train(i,:);
    dmax = max(row);
    dmin = min(row);
    % find k, b such that: 1 = k*dmax+b -1 = k*dmin+b
    % if dmax=dmin => k=0,b=0;
    if dmax==dmin,
        k = 0;
        b = 0;
    else
        k = 2/(dmax-dmin);
        b = 1-k*dmax;
    end
    fvScale(i,:) = k*data_train(i,:)+b;
    s1(i) = k;
    s2(i) = b;
end
