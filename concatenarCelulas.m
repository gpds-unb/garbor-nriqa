function matrizConcatenadaComCentroides = concatenarCelulas(vetorComCentroides)

% dict, saida da fun��o scriptLIVE_dict
numImg = size(vetorComCentroides,1);
matrizConcatenadaComCentroides = [];
for i=1:numImg
    matrizConcatenadaComCentroides = cat(1,matrizConcatenadaComCentroides,vetorComCentroides{i,1}); 
end
